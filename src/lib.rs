extern crate failure;
#[macro_use]
extern crate failure_derive;
extern crate itertools;

pub mod day1;
pub mod day2;
