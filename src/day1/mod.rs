use std::ops::Add;
use itertools::Itertools;

#[derive(Debug, Clone, Eq, PartialEq, Fail)]
pub enum ErrorKind {
    #[fail(display = "Given string contained {} which is not a digit.", ch)] InvalidString { ch: char },
}

pub type Result<T> = ::std::result::Result<T, ErrorKind>;

pub fn captcha(string: &str, interval: usize) -> Result<u32> {
    let string = string.trim();
    let char_cycle = string.chars().cycle();
    let char_pairs = char_cycle
        .clone()
        .take(string.len())
        .zip(char_cycle.skip(interval));

    char_pairs
        .filter(|&(ch, other)| ch == other)
        .map(|(ch, _)| {
            ch.to_digit(10)
                .ok_or_else(|| ErrorKind::InvalidString { ch })
        })
        .fold_results(0, u32::add)
}

pub fn captcha_next(string: &str) -> Result<u32> {
    captcha(string, 1)
}

pub fn captcha_half(string: &str) -> Result<u32> {
    captcha(string, string.len() / 2)
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &'static str = include_str!("input");

    #[test]
    fn next() {
        for &(case_in, case_out) in &[("1122", 3), ("1111", 4), ("1234", 0), ("91212129", 9)] {
            assert_eq!(captcha_next(case_in).unwrap(), case_out);
        }

        let answer = captcha_next(INPUT).unwrap();
        println!("Day 1: First half answer: {}", answer);
    }

    #[test]
    fn half() {
        for &(case_in, case_out) in &[
            ("1212", 6),
            ("1221", 0),
            ("123425", 4),
            ("123123", 12),
            ("12131415", 4),
        ] {
            assert_eq!(captcha_half(case_in).unwrap(), case_out);
        }

        let answer = captcha_half(INPUT).unwrap();
        println!("Day 1: Second half answer: {}", answer);
    }
}
