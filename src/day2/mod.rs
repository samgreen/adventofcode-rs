use std::result;
use std::ops::Add;
use std::io;
use std::io::prelude::*;
use itertools::Itertools;

#[derive(Debug, Fail)]
pub enum ErrorKind {
    #[fail(display = "Given input was not a valid spreadsheet. Line: {}", line)]
    InvalidInput {
        line: String,
    },
    #[fail(display = "An IO error occurred. {}", error)] Io {
        #[cause] error: io::Error,
    },
}

pub type Result<T> = ::std::result::Result<T, ErrorKind>;

pub fn spreadsheet_checksum<B: BufRead>(buf: B) -> Result<u32> {
    buf.lines()
        .map(|line| line.map_err(|error| ErrorKind::Io { error }))
        // Filter out empty lines
        .filter(|line| match *line {
            Ok(ref line) => !line.is_empty(),
            Err(_) => true,
        })
        .map(|line| line.and_then(|line| {
            let diff = line.split_whitespace()
                .map(|cell| cell.parse::<u32>())
                .fold_results((u32::max_value(), u32::min_value()), |(min, max), n| {
                    (min.min(n), max.max(n))
                })
                .and_then(|(min, max)| Ok(max - min));
            diff.map_err(|_| ErrorKind::InvalidInput { line })
        }))
        .fold_results(0, Add::add)
}

pub fn spreadsheet_checksum_even<B: BufRead>(buf: B) -> Result<u32> {
    buf.lines()
        .map(|line| line.map_err(|error| ErrorKind::Io { error }))
        // Filter out empty lines
        .filter(|line| match *line {
            Ok(ref line) => !line.is_empty(),
            Err(_) => true,
        })
        .map(|line| line.and_then(|line| {
            let nums = line.split_whitespace()
                .map(|cell| cell.parse::<u32>())
                .collect::<result::Result<Vec<u32>, _>>();
            nums.ok().and_then(|nums| {
                for (lhs, rhs) in nums.iter().tuple_combinations() {
                    if lhs % rhs == 0 {
                        return Some(lhs / rhs);
                    } else if rhs % lhs == 0 {
                        return Some(rhs / lhs);
                    }
                }
                None
            }).ok_or_else(|| ErrorKind::InvalidInput { line })
        }))
        .fold_results(0, Add::add)
}

#[cfg(test)]
mod tests {
    use std::io::{BufReader, Cursor};
    use super::*;

    const INPUT: &'static str = include_str!("input");

    #[test]
    fn first_half() {
        let test_case = BufReader::new(Cursor::new("5 1 9 5\n7 5 3\n2 4 6 8"));
        assert_eq!(spreadsheet_checksum(test_case).unwrap(), 18);

        let input = BufReader::new(Cursor::new(INPUT));
        let answer = spreadsheet_checksum(input).unwrap();
        println!("Day 2: First half answer: {}", answer);
    }

    #[test]
    fn second_half() {
        let test_case = BufReader::new(Cursor::new("5 9 2 8\n9 4 7 3\n3 8 6 5"));
        assert_eq!(spreadsheet_checksum_even(test_case).unwrap(), 9);

        let input = BufReader::new(Cursor::new(INPUT));
        let answer = spreadsheet_checksum_even(input).unwrap();
        println!("Day 2: Second half answer: {}", answer);
    }
}
